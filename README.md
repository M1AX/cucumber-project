**Installation**




1. Make sure you have JAVA 8 or above installed to run this test.
If you don't have go https://www.java.com/en/download/manual.jsp

2. Install IDE for your system (ex.IntelliJ IDEA or comparable)

3. Install plugins:

- Cucumber for java
- Gherkin
- Lombok



**Get the code**

```
git clone https://gitlab.com/M1AX/cucumber-project.git
cd cucumber-project
```



**The project directory structure**

The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:

```
src
  + main
  + test
    + java                          Test runners and supporting code
    + resources
      + features                    Feature files
```



**Adding the Cucumber dependency**

```
<dependency>
    <groupId>net.serenity-bdd</groupId>
    <artifactId>serenity-core</artifactId>
    <version>2.0.38</version>
    <scope>test</scope>
    <exclusions>
        <exclusion>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-core</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>net.serenity-bdd</groupId>
    <artifactId>serenity-cucumber4</artifactId>
    <version>1.0.21</version>
    <scope>test</scope>
    </dependency>
<dependency>
    <groupId>io.cucumber</groupId>
    <artifactId>cucumber-java</artifactId>
    <version>4.2.0</version>
</dependency>
<dependency>
    <groupId>io.cucumber</groupId>
    <artifactId>cucumber-junit</artifactId>
    <version>4.2.0</version>
</dependency>
```



**Adding new tests**

Create a new scenario with extension .features
Create definition steps for each action `@Given @When @Then`

**Executing the tests**

To run the sample project, you can either just run the CucumberTestSuite test runner class, or run either mvn verify or gradle test from the command line.

`\$ mvn clean verify`

**Please note**

In the current structure, DTO wasn't implemented due to a small project and will be used when extending endpoints
