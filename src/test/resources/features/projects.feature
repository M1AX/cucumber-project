# new feature
# Tags: optional

Feature: Projects

  In order to work with projects
  An authorized user
  Should be able to get/create/delete projects

  Scenario: The authorized user gets all projects
    When the authorized user requests all projects
    Then user get 200 status code for project

  Scenario: The unauthorized user can't get projects
    When the unauthorized user requests all projects
    Then user get 401 status code for project

  Scenario: The authorized user can create a new project
    When the authorized user create a new project with name newCreatedProject
    Then user get 201 status code for project
    Then project with name newCreatedProject should be created
    Then the authorized user delete the project

  Scenario: The authorized user can't create a new project with empty required field
    When the authorized user create a new project with name ""
    Then user get 400 status code for project

  Scenario: The authorized user can't create a new project with already existing name
    When the authorized user create a new project with name cucumber-project
    Then user get 400 status code for project

  Scenario: The authorized user can delete a new project
    When the authorized user create a new project with name projectToDelete
    Then the authorized user delete the project