# new feature
# Tags: optional

Feature: Groups

  In order to see groups
  An authorized user
  Should be able to get list of all groups

  Scenario: The authorized user gets all groups
    When the authorized user requests all groups
    Then user get 200 status code for group

  Scenario: The unauthorized user can't gets all groups
    When the unauthorized user requests all groups
    Then user get 401 status code for group

  Scenario: The authorized user can create a new group
    When user create a new group with name newCreatedGroup and path newCreatedPath
    Then user get 201 status code for group
    And the authorized user requests all groups
    Then response body should contain:
      | web_url                                  | name            | path           |
      | https://gitlab.com/groups/newCreatedPath | newCreatedGroup | newCreatedPath |

  Scenario: The authorized user can't create a new group with empty required field
    When user create a new group with name "" and path ""
    Then user get 400 status code for group

  Scenario: The authorized user can't create a new group with already existing path
    When user create a new group with name duplicatedGroup and path duplicatedPath
    Then user get 201 status code for group
    And user create a new group with name duplicatedGroup and path duplicatedPath
    Then user get 400 status code for group

  Scenario: Get list of owned groups for authenticated user
    When user requests owned groups
    Then user get 404 status code for group

  Scenario: The authorized user can update a newly created group
    When user create a new group with name groupToUpdate and path pathToUpdate
    And user updates created group with name groupIsUpdated and path pathIsUpdated
    And the authorized user requests all groups
    Then response body should contain:
      | web_url                                 | name           | path          |
      | https://gitlab.com/groups/pathIsUpdated | groupIsUpdated | pathIsUpdated |

  Scenario: The authorized user can delete a newly created group
    When user create a new group with name groupToDelete and path pathToDelete
    And authorized user deletes the group
    Then user get 202 status code for group

  Scenario: The unauthorized user can't delete a newly created group
    When user create a new group with name groupNotDelete and path pathNotDelete
    And unauthorized user deletes the group
    Then user get 401 status code for group

  Scenario: The authorized user can get a list of access requests for a group
    When user create a new group with name groupAccessRequest and path pathAccessRequest
    And authorized user requests access groups request
    Then user get 200 status code for group
    And response body should contain:
      | web_url                                     | name               | path              |
      | https://gitlab.com/groups/pathAccessRequest | groupAccessRequest | pathAccessRequest |

  Scenario: The unauthorized user can't create a requests access for the authenticated user to a group
    When user create a new group with name groupNoAccessRequest and path pathNoAccessRequest
    And unauthorized user requests access groups request
    Then user get 401 status code for group

  Scenario: Authorized user adds a member to a group with wrong level
    When user create a new group with name groupForNewMember and path pathForNewMember
    And user requests group with access_level  and user_id 8159116
    Then user get 400 status code for group
    And error message access_level [is not included in the list]

  Scenario: Authorized user adds an owner to a group
    When user create a new group with name groupForOwner and path pathForOwner
    And user requests group with access_level  and user_id 8079027
    Then user get 409 status code for group
    And user get the message Member already exists