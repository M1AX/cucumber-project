package constants;

public enum RestApiEndPoints {
    GROUPS("/groups"),
    GROUPS_OWNED("/groups/owned"),
    ACCESS_GROUPS("/access_requests"),
    MEMBERS("/members"),
    PROJECTS("/projects");

    private final String endPoint;

    RestApiEndPoints(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }
}
