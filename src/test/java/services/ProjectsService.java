package services;


import config.EnvConfig;
import constants.RestApiEndPoints;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class ProjectsService {
    EnvConfig env = EnvConfig.fromConfigFile();

    public Response getProjects(String token) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.PROJECTS.getEndPoint();
        return given()
                .formParam("private_token", token)
                .accept(ContentType.JSON)
                .when().get(url);
    }

    public Response createProject(String token, String name) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.PROJECTS.getEndPoint();
        return given()
                .formParam("private_token", env.getAuthToken())
                .and()
                .formParam("name", name)
                .accept(ContentType.JSON)
                .when().post(url);
    }

    public Response deleteProject(String token, int id) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.PROJECTS.getEndPoint() + "/" + id;
        return given()
                .formParam("private_token", env.getAuthToken())
                .and()
                .formParam("name", id)
                .accept(ContentType.JSON)
                .when().delete(url);
    }
}
