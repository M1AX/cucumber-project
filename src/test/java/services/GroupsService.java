package services;

import config.EnvConfig;
import constants.RestApiEndPoints;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class GroupsService {

    EnvConfig env = EnvConfig.fromConfigFile();

    public Response getGroups(String token) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint();
        return given()
                .formParam("private_token", token)
                .expect().defaultParser(Parser.JSON)
//                .accept(ContentType.JSON)
                .when().get(url);
    }

    public Response getAccessGroups(String groupId, String token) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint() + "/" + groupId + RestApiEndPoints.ACCESS_GROUPS.getEndPoint();
        return given()
                .formParam("private_token", token)
                .accept(ContentType.JSON)
                .when().get(url);
    }

    public Response createAccessGroups(String groupId, String token) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint() + "/" + groupId + RestApiEndPoints.ACCESS_GROUPS.getEndPoint();
        return given()
                .formParam("private_token", token)
                .accept(ContentType.JSON)
                .when().post(url);
    }

    public Response createGroup(String name, String path) {
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint();

        return given()
                .formParam("private_token", env.getAuthToken())
                .and()
                .formParam("name", name)
                .formParam("path", path)
                .accept(ContentType.JSON)
                .when().post(url);
    }

    public Response addMemberToTheGroup(String groupId, String access_level, String user_id) {
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint() + "/" + groupId + RestApiEndPoints.MEMBERS.getEndPoint();

        return given()
                .formParam("private_token", env.getAuthToken())
                .formParam("access_level", access_level)
                .formParam("user_id", user_id)
                .accept(ContentType.JSON)
                .when().post(url);
    }

    public Response updateGroup(String groupId, String name, String path) {
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint() + "/" + groupId;

        return given()
                .formParam("private_token", env.getAuthToken())
                .and()
                .formParam("name", name)
                .formParam("path", path)
                .accept(ContentType.JSON)
                .when().put(url);
    }

    public Response deleteGroup(String groupId, String token) {
        if (token.equals("authorized")) token = env.getAuthToken();
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS.getEndPoint() + "/" + groupId;

        return given()
                .formParam("private_token", token)
                .accept(ContentType.JSON)
                .when().delete(url);
    }

    public Response getGroupsOwned() {
        String url = env.getRestApiBaseUrl() + RestApiEndPoints.GROUPS_OWNED.getEndPoint();
        return given()
                .formParam("private_token", env.getAuthToken())
                .accept(ContentType.JSON)
                .when().get(url);
    }

    public void deleteAllGroups() {
        List<Integer> groupId = getGroups(env.getAuthToken()).jsonPath().get("id");
        for (Integer integer : groupId) deleteGroup(Integer.toString(integer), env.getAuthToken());
    }
}
