package config;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;


public class EnvConfig {

    private final EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

    private String authToken;
    private String restApiBaseUrl;

    public static EnvConfig fromConfigFile() {
        return new EnvConfig();
    }

    public EnvConfig() {
        restApiBaseUrl = EnvironmentSpecificConfiguration.from(variables).getProperty("restapi.base.url");
        authToken = EnvironmentSpecificConfiguration.from(variables).getProperty("authToken");
    }

    public String getRestApiBaseUrl() {
        return restApiBaseUrl;
    }

    public String getAuthToken() {
        return authToken;
    }
}
