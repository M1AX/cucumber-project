package util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class ApiResponseHelper {


    public static void validateResponse(DataTable dataTable, Response response) {
        List<Map<String, String>> myList = dataTable.asMaps();
        String groups = response.getBody().asString();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<HashMap<String, String>>>() {
        }.getType();
        List<HashMap<String, String>> map = gson.fromJson(groups, listType);
        myList.forEach(checkMapEntry -> {
            checkMapEntry.entrySet().forEach(checkEntry -> {
                map.forEach(resultMapEntry -> {
                    String resultValue = resultMapEntry.get(checkEntry.getKey());
                    assertReflectionEquals(checkEntry.getValue(), resultValue);
                });
            });
        });
    }
}
