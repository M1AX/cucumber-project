package testsuite.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import services.GroupsService;
import util.ApiResponseHelper;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class GroupsSteps {

    @Before
    public void prepareTests() {
        groupsService.deleteAllGroups();
    }

    @Steps
    GroupsService groupsService;
    Response getGroupsRequest;

    @When("the (.*) user requests all groups")
    public void getGroupsRequest(String token) {
        getGroupsRequest = groupsService.getGroups(token);
    }

    @When("(.*) user requests access groups request")
    public void getAccessGroupsRequest(String token) {
        getGroupsRequest = groupsService.getAccessGroups(getGroupId(), token);
    }

    @When("user with (.*) token create access groups request")
    public void createAccessGroupsRequest(String token) {
        getGroupsRequest = groupsService.createAccessGroups(getGroupId(), token);
    }

    @When("user status group id")
    public String getGroupId() {
        return getGroupsRequest.jsonPath().getString("id");
    }

    @When("user create a new group with name (.*) and path (.*)")
    public void createGroupsRequest(String name, String path) {
        getGroupsRequest = groupsService.createGroup(name, path);
    }

    @When("user requests group with access_level (.*) and user_id (.*)")
    public void addMemberToTheGroup(String access_level, String user_id) {
        getGroupsRequest = groupsService.addMemberToTheGroup(getGroupId(), access_level, user_id);
    }

    @When("user updates created group with name (.*) and path (.*)")
    public void updateGroupsRequest(String name, String path) {
        getGroupsRequest = groupsService.updateGroup(getGroupId(), name, path);
    }

    @When("(.*) user deletes the group")
    public void deleteGroupsRequest(String token) {
        getGroupsRequest = groupsService.deleteGroup(getGroupId(), token);
    }

    @Then("user get (.*) status code for group")
    public void getGroupsResponse(int statusCode) {
        assertReflectionEquals(statusCode, getGroupsRequest.getStatusCode());
    }

    @When("user requests owned groups")
    public void getGroupsOwnedRequest() {
        getGroupsRequest = groupsService.getGroupsOwned();
    }

    @Then("error message access_level (.*)")
    public void getAccessErrorMessage(String message) {
        assertReflectionEquals(message, getGroupsRequest.then().extract().response().jsonPath().getString("message.access_level"));
    }

    @Then("user get the message (.*)")
    public void getTheMessage(String message) {
        assertReflectionEquals(message, getGroupsRequest.jsonPath().getString("message"));
    }

    @Then("response body should contain:")
    public void getTheMessages(DataTable dataTable) {
        ApiResponseHelper.validateResponse(dataTable, getGroupsRequest);
    }
}
