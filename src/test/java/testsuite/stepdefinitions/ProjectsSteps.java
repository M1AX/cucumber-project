package testsuite.stepdefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import services.ProjectsService;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class ProjectsSteps {

    @Steps
    ProjectsService projectsService;
    Response getProjectResponse;

    @When("the (.*) user requests all projects")
    public void getGroupsRequest(String token) {
        getProjectResponse = projectsService.getProjects(token);
    }

    @When("the (.*) user create a new project with name (.*)")
    public void createProjectRequest(String token, String name) {
        getProjectResponse = projectsService.createProject(token, name);
    }

    @Then("user get (.*) status code for project")
    public void getProjectResponse(int statusCode) {
        assertReflectionEquals(statusCode, getProjectResponse.getStatusCode());
    }

    @Then("project with name (.*) should be created")
    public void getProjectName(String name) {
        assertReflectionEquals(name, getProjectResponse.jsonPath().getString("name"));
    }

    @When("the (.*) user delete the project")
    public void deleteProjectRequest(String token) {
        int id = getProjectResponse.jsonPath().getInt("id");
        getProjectResponse = projectsService.deleteProject(token, id);
    }
}
